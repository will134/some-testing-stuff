#!/usr/bin/env node

/**
 * @file        events.js
 *              Sample node application showing how to deploy a DCP job whilst receiving
 *              events describing the current state of the job, processing results
 *              as they are received, and so on.
 *
 *              Note: Your keystore should be placed in your home directory in .dcp/default.keystore.
 *              When using the dcp-client API in NodeJS, this keystore will be used for communicating over DCP.
 *
 * @author      Wes Garland, wes@kingsds.network
 * @date        Aug 2019, April 2020
 */

//const SCHEDULER_URL = new URL('http://scheduler.elijah.office.kingsds.network');
const SCHEDULER_URL = new URL('http://scheduler.will.office.kingsds.network');

 function testwork(){
  const colsRows = 25; 
  const frame_size = colsRows * 3; //75
  const frames = 4;
  const R = 128;
  const G = 0;
  const B = 128;

  //check to make sure we're inside the protocol/worker
  if (typeof progress == 'function'){
    progress();
  }

  var mainarr = new Uint8Array(frame_size * frames); //300

  for (i = 0; i < frames; i+= 1){
    var outerarr = new Uint8Array(frame_size);

    for (k = 0; k < colsRows; k+= 1){
      var innerarr = new Uint8Array([R, G , B]);
      outerarr.set(innerarr, k * 3); //up to 75
    }

    mainarr.set(outerarr, i * frame_size);

  }
  return mainarr;
}

var temp = testwork();
console.log(temp);


 /** Main program entry point */
 async function main() {
    const compute = require('dcp/compute');
    const wallet = require('dcp/wallet');
    let startTime;
    const singleslice = [1]
 
    const job = compute.for(
      singleslice,
      testwork
    );
 
   job.on('accepted', () => {
     console.log(` - Job accepted by scheduler, waiting for results`);
     console.log(` - Job has id ${job.id}`);
     startTime = Date.now();
   });
 
   job.on('readystatechange', (arg) => {
     console.log(`new ready state: ${arg}`);
   });
 
   job.on('result', (ev) => {
     console.log(
       ` - Received result for slice ${ev.sliceNumber} at ${
         Math.round((Date.now() - startTime) / 100) / 10
       }s`,
     );
     console.log(ev.result);
   });

 
   job.public.name = 'Minimal Test Case for Web Worker UInt8Array Issue';
 
   const ks = await wallet.get(); /* usually loads ~/.dcp/default.keystore */
   job.setPaymentAccountKeystore(ks);

   const results = await job.localExec(compute.marketvalue);
   console.log(Array.from(results));
  }
 
 /* Initialize DCP Client and run main() */
 require('dcp-client')
   .init(SCHEDULER_URL)
   .then(main)
   .catch(console.error)
 
