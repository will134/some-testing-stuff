#!/usr/bin/env node

const SCHEDULER_URL = new URL('http://scheduler.will.office.kingsds.network');
//const SCHEDULER_URL = new URL('https://scheduler.distributed.computer');

/** Main program entry point */
async function main() {
  const compute = require('dcp/compute');
  const wallet = require('dcp/wallet');
  let startTime;

  let jobArray = [];
  let pArray = [];

  function workFun (word) {
    progress();
    return word.toUpperCase();
  }

  const words = ['hello', 'world', 'my', 'name', 'is', 'will', 'pringle', 'eight', 'nine', 'ten'];

  // get array of jobs
  for(let i = 0; i < 10; i++) {
    jobArray.push(compute.for([words[i % words.length]], workFun));
  }

  // add event listeners to each job
  for (let i in jobArray) {
    jobArray[i].on('accepted', () => {
      console.log(` - Job ${i} accepted by scheduler, waiting for results`);
      console.log(` - Job ${i} has id ${jobArray[i].id}`);
      startTime = Date.now();
    });

    jobArray[i].on('readystatechange', (arg) => {
      console.log(` - Job ${i} new ready state: ${arg}`);
    });

    jobArray[i].on('result', (ev) => {
      console.log(` - Job ${i} Received result "${ev.result}" for slice ${ev.sliceNumber} at ${Math.round((Date.now() - startTime) / 100) / 10}s`);
    });

    jobArray[i].public.name = `MANY JOBS - JOB #${i}`;
  }

  const ks = await wallet.get(); /* usually loads ~/.dcp/default.keystore */
  
  for (let i in jobArray) {
    jobArray[i].setPaymentAccountKeystore(ks);
    pArray.push(jobArray[i].exec());
  }

  const results = await Promise.all(pArray);
  console.log(results);
}

/* Initialize DCP Client and run main() */
require('dcp-client')
  .init(SCHEDULER_URL)
  .then(main)
  .catch(console.error)
  .finally(process.exit);
