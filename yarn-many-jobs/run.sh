#!/usr/bin/env bash

echo "" > output.txt
echo "" > error.txt

for n in {0..10}; do
  ./yarn.js >> output.txt 2>> error.txt &
  sleep 1 # sleep seconds

done
