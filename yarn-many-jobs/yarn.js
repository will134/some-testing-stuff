#!/usr/bin/env node

/**
 * @file        events.js
 *              Sample node application showing how to deploy a DCP job whilst receiving
 *              events describing the current state of the job, processing results
 *              as they are received, and so on.
 *
 *              Note: Your keystore should be placed in your home directory in .dcp/default.keystore.
 *              When using the dcp-client API in NodeJS, this keystore will be used for communicating over DCP.
 *
 * @author      Wes Garland, wes@kingsds.network
 * @date        Aug 2019, April 2020
 */

const SCHEDULER_URL = new URL('http://scheduler.yarn.office.kingsds.network');


/** Main program entry point */
async function main() {
  const compute = require('dcp/compute');
  const wallet = require('dcp/wallet');
  const computeGroups = require('dcp/compute-groups');
  let startTime;
  const job = compute.for(
    1, 50,
    async (colour) => {
      progress(0);
      let sum = 0;
      for (let i = 0; i < 10; i++) {
        sum += Math.random();
      }
      return 0;
      
    }, 
  
  );

  job.on('accepted', () => {
    console.log('accepted')
  });

  job.on('readystatechange', (arg) => {
    console.log(`new ready state: ${arg}`);
  });

  job.on('result', (ev) => {
    console.log(
      ` - Received result for slice ${ev.sliceNumber} at ${
        Math.round((Date.now() - startTime) / 100) / 10
      }s`,
    );
    // console.log(` * Wow! ${ev.result.colour} is such a pretty colour!`);
  });

  // job.on('complete', (complete) => {
  //   console.log('complete')
  // })

  job.on('error', (err) => {
    console.log("got an error:")
    console.error(err)
  })

  // job.on('status', (ev) => {
  //   console.log("got a status:")
  //   console.log(ev)
  // })
  job.on('cancel', (ev) => {
    console.log("got the cancel event");
    console.log(ev)
  })
  job.on('ENOFUNDS', (ev) => {
    console.log("HEY YOU DON'T GOT NO MONEY")
  })
  
  job.public.name = 'super job';
  // job.requires('./testFile')
  job.requirements.discrete = true;
  const ks = await wallet.get(); /* usually loads ~/.dcp/default.keystore */
  console.log('launched')
  job.setPaymentAccountKeystore(ks);
  const results = await job.exec();
  
  console.log('complete');
}

/* Initialize DCP Client and run main() */
require('../dcp-client')
  .initSync(SCHEDULER_URL)
main()
  //.catch(console.error)
  //.finally(process.exit);
