#!/usr/bin/env bash

echo "Jobs launched: $(grep -c "launched" "output.txt")"
echo "Jobs accepted: $(grep -c "accepted" "output.txt")"
echo "Jobs complete: $(grep -c "complete" "output.txt")"
