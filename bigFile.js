#!/usr/bin/env node

const SCHEDULER_URL = new URL('http://scheduler.will.office.kingsds.network');
//const SCHEDULER_URL = new URL('http://scheduler.staging.office.kingsds.network');
//const SCHEDULER_URL = new URL('https://scheduler.distributed.computer');

const winj = require('winj');

debugger;

/** Main program entry point */
async function main() {
  const compute = require('dcp/compute');
  const wallet = require('dcp/wallet');
  let startTime;

  function workFn() {
    progress();
    const bigFile = WINJ_EMBED_FILE_AS_BIN('./clang');
    progress();
    return 'it worked';
  }

  let stringifiedWorkFn;
  stringifiedWorkFn = await winj.embedFilesAsBinary(workFn);


  let theWorkFn;

  if (stringifiedWorkFn) {
    theWorkFn = stringifiedWorkFn;
  } else {
    theWorkFn = function (element) {
      progress(1);
      return element;
    }
  }

  const job = compute.for(
    ['red'],
    theWorkFn, 
  );

//  debugger;
//  job.requires('./clang');
//  debugger;

  job.on('accepted', () => { console.log(` - Job ${job.id} accepted`); startTime = Date.now(); });
  job.on('readystatechange', (arg) => { console.log(`new ready state: ${arg}`) });
  job.on('result', (ev) => {
    const time = Math.round((Date.now() - startTime) / 100) / 10;
    console.log(` - Received result for slice ${ev.sliceNumber} at ${time}s`);
    console.log(` * Wow! ${ev.result.colour} is such a pretty colour!`);
  });

  job.public.name = 'one job - red';
  console.log(job.computeGroups);

  const ks = await wallet.get(); /* usually loads ~/.dcp/default.keystore */
  job.setPaymentAccountKeystore(ks);

// const results = await job.localExec();
  const results = await job.exec();
  console.log('results=', Array.from(results));
}

/* Initialize DCP Client and run main() */
require('dcp-client')
  .init(SCHEDULER_URL)
  .then(main)
  .catch(console.error)
  .finally(process.exit);
