#!/usr/bin/env node

const SCHEDULER_URL = new URL('http://scheduler.will.office.kingsds.network');
//const SCHEDULER_URL = new URL('http://scheduler.staging.office.kingsds.network');
//const SCHEDULER_URL = new URL('https://scheduler.distributed.computer');

/** Main program entry point */
async function main() {
  const compute = require('dcp/compute');
  const wallet = require('dcp/wallet');
  let startTime;

  const job = compute.for(
    ['red', 'green', 'blue'],
    (colour) => {
      progress(0);
      let sum = 0;
      for (let i = 0; i < 100000; i += 1) {
        progress(i / 100000);
        sum += Math.random();
      }
      return { colour, sum };
    },
  );

  const ks = await wallet.get(); /* usually loads ~/.dcp/default.keystore */
  job.setPaymentAccountKeystore(ks);

  job.on('accepted', () => { console.log(` - Job ${job.id} accepted`); startTime = Date.now(); });
  job.on('readystatechange', (arg) => { console.log(`new ready state: ${arg}`) });
  job.on('result', (ev) => {
    const time = Math.round((Date.now() - startTime) / 100) / 10;
    console.log(` - Received result ${ev.result.colour} for slice ${ev.sliceNumber} at ${time}s`);
    
    // compute.status form one --- prints undefined 
    const p1 = compute.status(job, ks);
    p1.then((res) => {
      console.log('ref1: ' + res); debugger;
    });

    // job status form two     --- prints nothing
    const f2Options = {
      //startTime: new Date(new Date().getFullYear(), 0, 1),
      endTime:   new Date(),
      startTime: new Date(Date.now() - 24*60*60*1000*365),
    }
    const p2 = compute.status(f2Options, ks);
    p2.then((res) => {
      console.log('ref2: ' + res); debugger;
    });

  });

  job.public.name = 'status.js - job.status tests';
  console.log(job.computeGroups);
  
  const execPromise = job.exec();

// ----------------- job status stuff -------------------
/*
  // job.status form one
  const resF1 = await compute.status(job, ks);
  console.log('exf1: ' + resF1); debugger;

  // job status form two
  const f2Options = {
    startTime: new Date(new Date().getFullYear(), 0, 1),
    endTime:   new Date(),
  }
  const resF2 = await compute.status(f2Options, ks);
  console.log('exf2: ' + resF2); debugger;
*/
// -------------- end of job status stuff ---------------

  const results = await execPromise;
  console.log('results=', Array.from(results));
}

/* Initialize DCP Client and run main() */
require('dcp-client')
  .init(SCHEDULER_URL)
  .then(main)
  .catch(console.error)
  .finally(process.exit);
